
-- Database: `qlsv`
--

-- --------------------------------------------------------

--
-- Table structure for table `dmkhoa`
--

CREATE TABLE `dmkhoa` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dmkhoa`
--

INSERT INTO `dmkhoa` (`MaKH`, `TenKhoa`) VALUES
('1', 'Công nghệ thông tin'),
('2', 'Kinh tế'),
('3', 'Xã hội');

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `sinhvien` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) DEFAULT NULL,
  `TenSV` varchar(15) DEFAULT NULL,
  `GioiTinh` char(1) DEFAULT NULL,
  `NgaySinh` datetime DEFAULT NULL,
  `NoiSinh` varchar(50) DEFAULT NULL,
  `DiaChi` varchar(50) DEFAULT NULL,
  `MaKH` varchar(6) DEFAULT NULL,
  `HocBong` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES
('1', 'Nguyen', 'Van A', '1', '0000-00-00 00:00:00', 'Ha Noi', 'Cau Giay', '2', 0),
('2', 'Nguyen', 'Van B', '1', '0000-00-00 00:00:00', 'Ha Noi', 'Cau Giay', '1', 0),
('3', 'Nguyen', 'Van C', '1', '0000-00-00 00:00:00', 'Ha Noi', 'Thanh Xuan', '1', 0),
('4', 'Nguyen', 'Van D', '1', '0000-00-00 00:00:00', 'Ha Noi', 'Thanh Xuan', '2', 0),
('5', 'Nguyen', 'Van E', '1', '0000-00-00 00:00:00', 'Ha Noi', 'Ba Dinh', '1', 0),
('6', 'Nguyen', 'Van F', '1', '0000-00-00 00:00:00', 'Ha Noi', 'Tay Ho', '3', 0),
('7', 'Nguyen', 'Van G', '1', '0000-00-00 00:00:00', 'Ha Noi', 'Cau Giay', '1', 0),
('8', 'Nguyen', 'Van H', '1', '0000-00-00 00:00:00', 'Ha Noi', 'Ha Dong', '2', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dmkhoa`
--
ALTER TABLE `dmkhoa`
  ADD PRIMARY KEY (`MaKH`);

--
-- Indexes for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`MaSV`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
